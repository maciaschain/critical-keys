;;; critical-keys.el --- A simple minor mode for Emacs / AUCTeX, that provides a series of shortcuts to work with the excellent LaTeX package reledmac (https://github.com/maieul/ledmac).

;; Author: Juan Manuel Macías <maciaschain at gmail dot com>
;; Keywords: reledmac, AUCTeX, critical edition
;; Homepage:

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

;;;###autoload

(define-minor-mode critical-keys-mode
  "Insert reledmac commands"
  :lighter "critical-keys"

  (defun letter-note ()
    (interactive)
    (defvar letter)
    (if (region-active-p)
	(progn
	  (save-excursion
	    (goto-char (region-end))
	    (insert (concat "}{\\"
			    (format "%s" letter)
			    "note[]{}}")))
	  (save-excursion
	    (goto-char (region-beginning))
	    (insert "\\edtext{")))
      (insert "\\edtext{")
      (save-excursion
	(insert (concat "}{\\"
			(format "%s" letter)
			"note[]{}}")))))

  (defun letter-note-lemma ()
    (interactive)
    (defvar letter)
    (if (region-active-p)
	(progn
	  (save-excursion
	    (goto-char (region-end))
	    (insert (concat "}{\\lemma{}\\"
			    (format "%s" letter)
			    "note[]{}}")))
	  (save-excursion
	    (goto-char (region-beginning))
	    (insert "\\edtext{")))
      (insert "\\edtext{")
      (save-excursion
	(insert (concat "}{\\lemma{}\\"
			(format "%s" letter)
			"note[]{}}")))))

  ;; edtext + Afootnote simple

  (defun afootnote ()
    (interactive)
    (setq letter "Afoot")
    (letter-note))

  ;; edtext + Aendnote simple

  (defun aendnote ()
    (interactive)
    (setq letter "Aend")
    (letter-note))

  (defun critical-a-note ()
    (interactive)
    (if (equal current-prefix-arg nil) ; no C-u
	(afootnote)
      (aendnote)))

  ;; edtext + Afootnote with lemma

  (defun afootnote-lemma ()
    (interactive)
    (setq letter "Afoot")
    (letter-note-lemma))

  ;; edtext + Aendnote with lemma

  (defun aendnote-lemma ()
    (interactive)
    (setq letter "Aend")
    (letter-note-lemma))

  (defun critical-a-note-lemma ()
    (interactive)
    (if (equal current-prefix-arg nil) ; no C-u
	(afootnote-lemma)
      (aendnote-lemma)))

  ;; edtext + Bfootnote simple

  (defun bfootnote ()
    (interactive)
    (setq letter "Bfoot")
    (letter-note))

  ;; edtext + Bendnote simple

  (defun bendnote ()
    (interactive)
    (setq letter "Bend")
    (letter-note))

  (defun critical-b-note ()
    (interactive)
    (if (equal current-prefix-arg nil) ; no C-u
	(bfootnote)
      (bendnote)))

  ;; edtext + Bfootnote with lemma

  (defun bfootnote-lemma ()
    (interactive)
    (setq letter "Bfoot")
    (letter-note-lemma))

  ;; edtext + Bendnote with lemma

  (defun bendnote-lemma ()
    (interactive)
    (setq letter "Bend")
    (letter-note-lemma))

  (defun critical-b-note-lemma ()
    (interactive)
    (if (equal current-prefix-arg nil) ; no C-u
	(bfootnote-lemma)
      (bendnote-lemma)))

  ;; edtext + Cfootnote simple

  (defun cfootnote ()
    (interactive)
    (setq letter "Cfoot")
    (letter-note))

  ;; edtext + Cendnote simple

  (defun cendnote ()
    (interactive)
    (setq letter "Cend")
    (letter-note))

  (defun critical-c-note ()
    (interactive)
    (if (equal current-prefix-arg nil) ; no C-u
	(cfootnote)
      (cendnote)))

  ;; edtext + Cfootnote with lemma

  (defun cfootnote-lemma ()
    (interactive)
    (setq letter "Cfoot")
    (letter-note-lemma))

  ;; edtext + Cendnote with lemma

  (defun cendnote-lemma ()
    (interactive)
    (setq letter "Cend")
    (letter-note-lemma))

  (defun critical-c-note-lemma ()
    (interactive)
    (if (equal current-prefix-arg nil) ; no C-u
	(cfootnote-lemma)
      (cendnote-lemma)))

  ;; any series

  (defun any-series-footnote ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: ")))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\"
			       (format "%s" series)
			       "footnote[]{}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\"
			   (format "%s" series)
			   "footnote[]{}}"))))))

  (defun any-series-footnote-killring ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: "))
	 (paste (car kill-ring)))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\"
			       (format "%s" series)
			       "footnote[]{"
			       (format "%s" paste)
			       "}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\"
			   (format "%s" series)
			   "footnote[]{}}"))))))

  (defun any-series-footnote-lemma ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: ")))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\lemma{}\\"
			       (format "%s" series)
			       "footnote[]{}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\lemma{}\\"
			   (format "%s" series)
			   "footnote[]{}}"))))))

  (defun any-series-footnote-lemma-killring ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: "))
	 (paste (car kill-ring)))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\lemma{}\\"
			       (format "%s" series)
			       "footnote[]{"
			       (format "%s" paste)
			       "}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\lemma{}\\"
			   (format "%s" series)
			   "footnote[]{}}"))))))

  (defun any-series-endnote ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: ")))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\"
			       (format "%s" series)
			       "endnote[]{}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\"
			   (format "%s" series)
			   "endnote[]{}}"))))))

  (defun any-series-endnote-killring ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: "))
	 (paste (car kill-ring)))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\"
			       (format "%s" series)
			       "endnote[]{"
			       (format "%s" paste)
			       "}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\"
			   (format "%s" series)
			   "endnote[]{}}"))))))

  (defun any-series-endnote-lemma ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: ")))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\lemma{}\\"
			       (format "%s" series)
			       "endnote[]{}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\lemma{}\\"
			   (format "%s" series)
			   "endnote[]{}}"))))))

  (defun any-series-endnote-lemma-killring ()
    (interactive)
    (let
	((series (read-from-minibuffer "Series: "))
	 (paste (car kill-ring)))
      (if (region-active-p)
	  (progn
	    (save-excursion
	      (goto-char (region-end))
	      (insert (concat  "}{\\lemma{}\\"
			       (format "%s" series)
			       "endnote[]{"
			       (format "%s" paste)
			       "}}")))
	    (save-excursion
	      (goto-char (region-beginning))
	      (insert "\\edtext{")))
	(insert "\\edtext{")
	(save-excursion
	  (insert (concat  "}{\\lemma{}\\"
			   (format "%s" series)
			   "endnote[]{}}"))))))

  ;; numbered region

  (defun numbered-region ()
    (interactive)
    (if (region-active-p)
	(progn
	  (save-excursion
	    (goto-char (region-end))
	    (newline 2)
	    (insert "\\endnumbering"))
	  (save-excursion
	    (goto-char (region-beginning))
	    (insert "\\beginnumbering")
	    (newline 2)))
      (insert "\\beginnumbering")
      (newline)
      (save-excursion
	(newline)
	(insert "\\endnumbering"))))

  ;; insert \pstart & \pend

  (defun pstart-pend ()
    (interactive)
    (if (region-active-p)
	(progn
	  (save-excursion
	    (goto-char (region-end))
	    (newline 2)
	    (insert "\\pend"))
	  (save-excursion
	    (goto-char (region-beginning))
	    (insert "\\pstart")
	    (newline 2)))
      (insert "\\pstart")
      (newline)
      (save-excursion
	(newline)
	(insert "\\pend"))))

  ;; delete extra trailing lines inside a region

  (defun clean-spurious-lines ()
    (save-excursion
      (goto-char (point-max))
      (when (looking-at-p "^$")
	(re-search-backward ".")
	(end-of-line)
	(delete-blank-lines)
	(delete-forward-char 1))))

  ;; numbered region & pstart + pend between paragraphs

  (defun ck-replace (before after)
    (interactive)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward before nil t)
	(replace-match after t nil))))

  (defun complete-numbered-region ()
    (interactive)
    (if (region-active-p)
	(progn
	  (narrow-to-region (region-beginning) (region-end))
	  (deactivate-mark)
	  (whitespace-cleanup)
	  (clean-spurious-lines)
	  (ck-replace "\n\n\n+" "\n\n")
	  (ck-replace "^$" "\\\\pend\\\\pstart")
	  (ck-replace "\\\\pend\\\\pstart" "\n\\\\pend\\\\pstart\n")
	  (save-excursion
	    (goto-char (point-max))
	    (newline 2)
	    (insert "\\pend\\endnumbering"))
	  (save-excursion
	    (goto-char (point-min))
	    (insert "\\beginnumbering\\pstart")
	    (newline 2))
	  (widen))
      (insert "\\beginnumbering\\pstart")
      (newline)
      (save-excursion
	(newline)
	(insert "\\pend\\endnumbering"))))

  ;; ido-edtext

  (defun element-edtext-list (a b)
    (save-restriction
      (narrow-to-region a b)
      (buffer-string)))

  (defun edtext-list ()
    (let
	((x (make-marker))
	 (y (make-marker)))
      (setq list-edtext nil)
      (save-excursion
	(goto-char (point-min))
	(while
	    (re-search-forward "\\\\edtext{" nil t)
	  (set-marker x (point))
	  (sp-end-of-sexp)
	  (set-marker y (point))
	  (add-to-list 'list-edtext (element-edtext-list x y))))))

  (defun create-edtext-list ()
    (edtext-list)
    (mapcar 'identity list-edtext))

  (defun ido-edtext (edtext)
    (interactive  (list (ido-completing-read "edtext list: " (create-edtext-list) nil t)))
    (goto-char (point-min))
    (occur (concat "\\\\edtext{" (format "%s" edtext))))

  ;; fold / unfold reledmac notes, lemmas and \xxref macros

  (defun ensure-fold-mode ()
    (unless (TeX-fold-mode 1)
      (TeX-fold-mode)))

  (defun fold-reledmac-notes-buffer ()
    (interactive)
    (ensure-fold-mode)
    (save-excursion
      (goto-char (point-min))
      (while
	  (re-search-forward (concat "\\(note\\|lemma\\|xxref\\)"  "\\(\\[.*\\]\\)*" "{") nil t)
	(TeX-fold-macro))))

  (defun fold-reledmac-notes-region ()
    (interactive)
    (ensure-fold-mode)
    (save-excursion
      (save-restriction
	(narrow-to-region (region-beginning) (region-end))
	(goto-char (point-min))
	(while
	    (re-search-forward (concat "\\(note\\|lemma\\|xxref\\)"  "\\(\\[.*\\]\\)*" "{") nil t)
	  (TeX-fold-macro)))))

  (defun fold-reledmac-note ()
    (interactive)
    (ensure-fold-mode)
    (let
	((a (make-marker))
	 (b (make-marker)))
      (re-search-backward "\\\\edtext" nil t)
      (set-marker a (point))
      (re-search-forward "{" nil t)
      (right-char 1)
      (sp-beginning-of-next-sexp)
      (sp-end-of-sexp)
      (right-char 1)
      (set-marker b (point))
      (save-restriction
	(narrow-to-region a b)
	(fold-reledmac-notes-buffer))
      (deactivate-mark)))

  (defun fold-reledmac-notes-buffer-all ()
    (interactive)
    (let
	((LaTeX-fold-macro-spec-list '((1 ("edtext")))))
      (ensure-fold-mode)
      (TeX-fold-buffer)
      (save-excursion
	(goto-char (point-min))
	(while
	    (re-search-forward (concat "\\(note\\|lemma\\|xxref\\)"  "\\(\\[.*\\]\\)*" "{") nil t)
	  (TeX-fold-macro)))))

  (defun fold-reledmac-notes-buffer-all-var ()
    (interactive)
    (ensure-fold-mode)
    (TeX-fold-buffer)
    (save-excursion
      (goto-char (point-min))
      (while
	  (re-search-forward (concat "\\(edtext\\|note\\|lemma\\|xxref\\)"  "\\(\\[.*\\]\\)*" "{") nil t)
	(TeX-fold-macro))))

  (defun unfold-reledmac-all ()
    (interactive)
    (TeX-fold-clearout-buffer))

  ;; edlabels

  (setq reledmac-label-list nil)

  (defun insert-edlabel-at-point ()
    (interactive)
    (let
	((label (read-from-minibuffer "label: ")))
      (insert (concat "\\edlabel{" label "}"))
      (add-to-list 'reledmac-label-list label)
      (save-excursion
	(add-file-local-variable 'reledmac-label-list (mapcar 'identity reledmac-label-list)))))

  (defun make-edlabel-list ()
    (mapcar 'identity reledmac-label-list))

  (defun ido-select-edlabel (label)
    (interactive  (list (ido-completing-read "Choose label: " (make-edlabel-list) nil t)))
    (insert (format "%s"  label)))

  )

;; key and menu mode map
(require 'easymenu)
(defvar critical-keys-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-Q") 'critical-a-note)
    (define-key map (kbd "M-W") 'critical-a-note-lemma)
    (define-key map (kbd "M-E") 'critical-b-note)
    (define-key map (kbd "M-R") 'critical-b-note-lemma)
    (define-key map (kbd "M-T") 'critical-c-note)
    (define-key map (kbd "M-Y") 'critical-c-note-lemma)
    (define-key map (kbd "M-A") 'numbered-region)
    (define-key map (kbd "M-S") 'pstart-pend)
    (define-key map (kbd "M-D") 'complete-numbered-region)
    (define-key map (kbd "C-x <f1>") 'any-series-footnote)
    (define-key map (kbd "C-x <f2>") 'any-series-footnote-lemma)
    (define-key map (kbd "C-x <f3>") 'any-series-endnote)
    (define-key map (kbd "C-x <f4>") 'any-series-endnote-lemma)
    (define-key map (kbd "C-x <f5>") 'any-series-footnote-killring)
    (define-key map (kbd "C-x <f6>") 'any-series-footnote-lemma-killring)
    (define-key map (kbd "C-x <f7>") 'any-series-endnote-killring)
    (define-key map (kbd "C-x <f8>") 'any-series-endnote-lemma-killring)
    (define-key map (kbd "C-x '") 'ido-edtext)
    (define-key map (kbd "C-c C-x l") 'insert-edlabel-at-point)
    (define-key map (kbd "C-x ?") 'ido-select-edlabel)
    (define-key map (kbd "C-c C-x b") 'fold-reledmac-notes-buffer)
    (define-key map (kbd "C-c C-x r") 'fold-reledmac-notes-region)
    (define-key map (kbd "C-c C-x n") 'fold-reledmac-note)
    (define-key map (kbd "C-c C-x x") 'fold-reledmac-notes-buffer-all)
    (define-key map (kbd "C-c C-x y") 'fold-reledmac-notes-buffer-all-var)
    (define-key map (kbd "C-c C-x u") 'unfold-reledmac-all)
    ;; menu items
    (easy-menu-define critical-keys-mode-menu map
      "Critical keys menu"
      '("Critical"
	["edtext-Afootnote" afootnote]
	["edtext-Afootnote-lemma" afootnote-lemma]
	["edtext-Bfootnote" bfootnote]
	["edtext-Bfootnote-lemma" bfootnote-lemma]
	["edtext-Cfootnote" cfootnote]
	["edtext-Cfootnote-lemma" cfootnote-lemma]
	["numbered-region" numbered-region]
	["pstart-pend" pstart-pend]
	["complete-numbered-region" complete-numbered-region]))
    map))

(add-to-list 'minor-mode-map-alist `(critical-keys-mode . ,critical-keys-mode-map) t)

(add-hook 'LaTeX-mode-hook 'critical-keys-mode)

(provide 'critical-keys-mode)

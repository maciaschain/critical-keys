critical-keys.el
================

Contents
--------

-   [Description](#description)
-   [Installation](#installation)
-   [Keys](#keys)
	-   [For any series](#for-any-series)
	-   [Ido-edtext](#ido-edtext)
	-   [Fold or unfold reledmac notes, lemmas and \'xxref\'
		macros](#fold-or-unfold-reledmac-notes-lemmas-and-xxref-macros)
	-   [For edlabels](#for-edlabels)
-   [A sample video](#a-sample-video)

Description
-----------

A simple minor mode for Emacs / AUCTeX, that provides a series
(extendable) of shortcuts to work with the excellent LaTeX package
[reledmac](https://github.com/maieul/ledmac).

Installation
------------

Put the `critical-keys.el` file in your `load-path` and add to your init
file the line:

``` {.example}
(load "critical-keys.el")
```

Keys
----

(Note: All commands are also accessible from the menu item \"Critical\".
When called with universal argument `C-u` insert end notes).

`M-Q`
:   It wraps the current region with the command
	`\edtext{<REGION>}{\Afootnote[]{}}` or inserts the empty command in
	a blank line

`M-W`
:   It wraps the current region with the command
	`\edtext{<REGION>}{\lemma{}\Afootnote[]{}}` or inserts the empty
	command in a blank line

`M-E`
:   It wraps the current region with the command
	`\edtext{<REGION>}{\Bfootnote[]{}}` or inserts the empty command in
	a blank line

`M-R`
:   It wraps the current region with the command
	`\edtext{<REGION>}{\lemma{}\Bfootnote[]{}}` or inserts the empty
	command in a blank line

`M-T`
:   It wraps the current region with the command
	`\edtext{<REGION>}{\Cfootnote[]{}}` or inserts the empty command in
	a blank line

`M-Y`
:   It wraps the current region with the command
	`\edtext{<REGION>}{\lemma{}\Cfootnote[]{}}` or inserts the empty
	command in a blank line

`M-A`
:   It places above and below the current region the commands
	`\beginnumbering ... \endnumbering` or inserts the commands in a
	blank line

`M-S`
:   It places above and below the current region the commands
	`\pstart ... \pend` or inserts the commands in a blank line

`M-D`
:   It places above and below the current region the commands
	`\beginnumbering\pstart ... \pend\endnumbering` and inserts a
	`\pend\pstart` between paragraphs in the current region (and removes
	extra empty lines too)

### For any series

`C-x <f1>`
:   It prompts for a footnote series

`C-x <f2>`
:   It prompts for a footnote series with a lemma command

`C-x <f3>`
:   It prompts for a endnote series

`C-x <f4>`

:   It prompts for a endnote series with a lemma command

`C-x <f5>`
:   It prompts for a footnote series and inserts the clipboard contents
	into the note

`C-x <f6>`
:   It prompts for a footnote series with a lemma command and inserts
	the clipboard contents into the note

`C-x <f7>`
:   It prompts for a endnote series and inserts the clipboard contents
	into the note

`C-x <f8>`

:   It prompts for a endnote series with a lemma command and inserts the
	clipboard contents into the note

### Ido-edtext

`C-x '`
:   It launches the command `ido-edtext` to navigate with `ido` through
	the `edtext` of the current document (requires `smartparens`)

### Fold or unfold reledmac notes, lemmas and \'xxref\' macros

(These functions require `TeX-fold-mode`)

`C-c C-x b`
:   `fold-reledmac-notes-buffer`

`C-c C-x r`
:   `fold-reledmac-notes-region`

`C-c C-x n`
:   `fold-reledmac-note` (only inside the current `\edtext` macro=)

`C-c C-x x`
:   `fold-reledmac-notes-buffer-all` (all macros are folded in the buffer, including `edtext`)

`C-c C-x y`
:   `fold-reledmac-notes-buffer-all-var` (all macros are folded in the buffer. The content of `edtext` is not shown. The string "[edtext]" appears instead)

`C-c C-x u`
:   `unfold-reledmac-all`

A sample gif:

![](fold-relecmac-bueno.gif)

### For edlabels

`C-c C-x l`
:   `insert-edlabel-at-point` It prompts for a label and inserts the reledmac macro `\edlabel{}`. Each label is stored in the document in a list as a local variable.

`C-x ?`
:   `ido-select-edlabel` It launches `ido` to navigate through a list of local labels

A sample gif:

![](selectlabel.gif)

A sample video
--------------

<https://vimeo.com/309654544>
